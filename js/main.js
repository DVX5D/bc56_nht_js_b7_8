// Tạo mảng và lưu các số nguyên vào mảng
var arrInt = [];
var newInt = 0;

document.querySelector('#btnAddNumber').onclick = function () {
    newInt = +document.querySelector('#addNumber').value;
    arrInt.push(newInt);
    document.querySelector('#newArray').innerHTML = arrInt;
    document.querySelector('#addNumber').value = '';
}

// Bài 1: Tính tổng các số dương trong mảng
document.querySelector('#calSumPosInt').onclick = function () {
    var sumPosInt = 0;
    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] > 0) {
            sumPosInt += arrInt[index];
        }
    }
    displayResult('#resultSumPosIny', sumPosInt);
}

// Xóa kết quả
document.querySelector('#delSesOne').onclick = function () {
    clearResult('#resultSumPosIny');
}

// Bài 2: Đếm có bao nhiêu số dương trong mảng
document.querySelector('#countPosInt').onclick = function () {
    var countPosInt = 0;
    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] > 0) {
            countPosInt++;
        }
    }
    displayResult('#resultCountPosInt', countPosInt);
}

// Xóa kết quả
document.querySelector('#delCountPosInt').onclick = function () {
    clearResult('#resultCountPosInt');
}

// Bài 3: Tìm số nhỏ nhất trong mảng
document.querySelector('#findMin').onclick = function () {
    var minInt = arrInt[0];
    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] < minInt) {
            minInt = arrInt[index];
        }
    }
    displayResult('#resultMin', minInt);
}

// Xóa kết quả
document.querySelector('#delMin').onclick = function () {
    clearResult('#resultMin');
}

// Bài 4: Tìm số dương nhỏ nhất trong mảng
document.querySelector('#findPosMin').onclick = function () {
    var arrPosInt = [];
    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] > 0) {
            arrPosInt.push(arrInt[index]);
        }
    }

    var minPosInt = arrPosInt[0];
    for (var indexTwo = 0; indexTwo < arrPosInt.length; indexTwo++) {
        if (arrPosInt[indexTwo] < minPosInt) {
            minPosInt = arrPosInt[indexTwo];
        }
    }
    displayResult('#resultPosMin', minPosInt);
}

// Xóa kết quả
document.querySelector('#delPosMin').onclick = function () {
    clearResult('#resultPosMin');
}

// Bài 5: Tìm số chẵn cuối cùng trong mảng. Nếu không có giá trị chẵn thì trả về -1
document.querySelector('#findEvenInt').onclick = function () {
    var evenInt = '';
    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] % 2 == 0) {
            evenInt = arrInt[index];
        }
    }

    if (evenInt == '') {
        displayResult('#resultEvenInt', -1);
    } else {
        displayResult('#resultEvenInt', evenInt);
    }
}

// Xóa kết quả
document.querySelector('#delEvenInt').onclick = function () {
    clearResult('#resultEvenInt');
}

// Bài 6: Đổi chỗ 2 giá trị trong mảng theo vị trí
document.querySelector('#swapIndex').onclick = function () {
    var indexFirst = +document.querySelector('#indexFirst').value;
    var indexSecond = +document.querySelector('#indexSecond').value;
    if (indexFirst != indexSecond && indexFirst < arrInt.length && indexSecond < arrInt.length && indexFirst >= 0 && indexSecond >= 0) {
        var newFirstVal = arrInt[indexSecond];
        var newSecondVal = arrInt[indexFirst];
        arrInt[indexFirst] = newFirstVal;
        arrInt[indexSecond] = newSecondVal;
        displayResult('#resultSwapIndex', arrInt);
    } else {
        displayResult('#resultSwapIndex', 'vui lòng nhập đúng vị trí giá trị trong mảng');
    }
}

// Xóa kết quả
document.querySelector('#delSwapIndex').onclick = function () {
    clearResult('#resultSwapIndex');
}

// Bài 7: Sắp xếp mảng theo thứ tự tăng dần
document.querySelector('#sortMinToMax').onclick = function () {
    var newArrInt = arrInt;
    newArrInt.sort();
    displayResult('#resultSortMinToMax', newArrInt);
}

// Xóa kết quả
document.querySelector('#delSortMinToMax').onclick = function () {
    clearResult('#resultSortMinToMax');
}

// Bài 8: Tìm số nguyên tố đầu tiên trong mảng
// Nếu mảng không có số nguyên tố thì trả về -1
document.querySelector('#findFirstFrime').onclick = function () {
    var primeNumber = 0;
    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] > 1) {
            var isPrime = true;
            for (var indexTwo = 2; indexTwo <= Math.sqrt(arrInt[index]); indexTwo++) {
                if (arrInt[index] % indexTwo === 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                primeNumber = arrInt[index];
            }
        }
    }
    if (isPrime == false) {
        displayResult('#resultFirstFrime', -1);
    } else {
        displayResult('#resultFirstFrime', primeNumber);
    }
}

// Xóa kết quả
document.querySelector('#delFirstFrime').onclick = function () {
    clearResult('#resultFirstFrime');
}

// Bài 9: Nhập thêm 1 mảng số thực
// Đếm số lượng số nguyên trong mảng
var arrFloat = [];

// Cho người dùng thêm 1 mảng số thực
document.querySelector('#btnAddFloatNumber').onclick = function () {
    var newFloatNumber = +document.querySelector('#addFloatNumber').value;
    arrFloat.push(newFloatNumber);
    document.querySelector('#newFloatArray').innerHTML = arrFloat;
    document.querySelector('#addFloatNumber').value = '';
}

// Đếm số lượng số nguyên trong mảng mà người dùng vừa nhập
document.querySelector('#countIntNumber').onclick = function () {
    var countIntNumber = 0;

    for (var index = 0; index < arrFloat.length; index++) {
        if (arrFloat[index] % 1 == 0) {
            countIntNumber++;
        }
    }

    displayResult('#resultIntNumber', countIntNumber);
}

// Xóa kết quả
document.querySelector('#delIntNumber').onclick = function () {
    clearResult('#resultIntNumber');
}

// Bài 10: So sánh số lượng số dương và số âm xem số nào nhiều hơn
document.querySelector('#comparePosNe').onclick = function () {
    var posNumber = 0;
    var negNumber = 0;

    for (var index = 0; index < arrInt.length; index++) {
        if (arrInt[index] < 0) {
            negNumber++;
        } else if (arrInt[index] > 0) {
            posNumber++;
        }
    }

    if (posNumber > negNumber) {
        displayResult('#resultComparePosNe', 'Số chẵn nhiều hơn số lẻ');
    } else if (posNumber < negNumber) {
        displayResult('#resultComparePosNe', 'Số lẻ nhiều hơn số chẵn');
    } else {
        displayResult('#resultComparePosNe', 'Số chẵn và số lẻ bằng số lượng với nhau');
    }
}

// Xóa kết quả
document.querySelector('#delComparePosNe').onclick = function () {
    clearResult('#resultComparePosNe');
}