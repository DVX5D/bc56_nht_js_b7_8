// Display the result
function displayResult(resultId, result) {
    var resultElement = document.querySelector(resultId);
    resultElement.style.display = 'block';
    resultElement.classList.add('alert', 'alert-success');
    resultElement.innerHTML = 'Kết quả là: ' + result;
}

// Clear the result
function clearResult(resultId) {
    var clearResultElement = document.querySelector(resultId);
    clearResultElement.innerHTML = '';
    clearResultElement.className = '';
    clearResultElement.style.display = 'none';
}